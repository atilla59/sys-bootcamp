
###### Localde Test etmek için

bu repository klonlayın ve alttaki komutu çalıştırın
```
$ docker-compose up
```
## Case 1
---
- 3 tane couchbase server *docker-compose* yardımı ile host üzerine kuruldu ve internal ip adresleri 10.5.0.0/16 subnetine ayarlarlandı (10.5.0.2 master olmak üzere,10.5.0.3, 10.5.0.4)
- master server 8091 portundan web ui girilerek kurulum yapıldı (data,index,query servisleri)
        **not**: buradaki master kavramı master slave değildir couchbase ölçeklendirmeyi homejen yapar

```
CONTAINER ID   IMAGE              COMMAND                  CREATED          STATUS          PORTS                                                                                                              NAMES
c5f2590584d4   couchbase/server   "/entrypoint.sh couc…"   16 minutes ago   Up 16 minutes   8091-8096/tcp, 11207/tcp, 11210-11211/tcp, 18091-18096/tcp                                                         couchbaseServer3
2176e3e84bde   couchbase/server   "/entrypoint.sh couc…"   16 minutes ago   Up 16 minutes   8091-8096/tcp, 11207/tcp, 11210-11211/tcp, 18091-18096/tcp                                                         couchbaseServer2
517e07d8cd79   couchbase/server   "/entrypoint.sh couc…"   16 minutes ago   Up 16 minutes   8095-8096/tcp, 0.0.0.0:8091-8094->8091-8094/tcp, 11207/tcp, 11211/tcp, 0.0.0.0:11210->11210/tcp, 18091-18096/tcp   couchbaseServer1
```

##### Ölçeklendirme

10.5.0.2 master serverımıza 10.5.0.3 ve 10.5.0.4 ip adreslerindeki serverları ekliyoruz

```
# couchbase-cli server-add -c 10.5.0.2:8091 --username Administrator \--password password --server-add http://10.5.0.4:8091 \--server-add-username Administrator --server-add-password password --services data,index,query
SUCCESS: Server added

# couchbase-cli server-add -c 10.5.0.2:8091 --username Administrator \--password password --server-add http://10.5.0.5:8091 \--server-add-username Administrator --server-add-password password --services data,index,query
SUCCESS: Server added

```
Kontrol edelim işlemleri

```
# couchbase-cli server-list -c 10.5.0.2:8091 --username Administrator \--password password
ns_1@10.5.0.2 10.5.0.2:8091 healthy active
ns_1@10.5.0.3 10.5.0.3:8091 healthy inactiveAdded
ns_1@10.5.0.4 10.5.0.4:8091 healthy inactiveAdded
```

inactive olanları rebalance ile active edelim
```
# couchbase-cli rebalance -c 10.5.0.2:8091 --username Administrator \--password password
Rebalancing
Bucket: 00/00 ()                                                                                                                                                                                0 docs remaining
[======================================================================================================================================================================================================] 100.00%
SUCCESS: Rebalance complete
```
Tekrar kontrol ediyoruz
```
# couchbase-cli server-list -c 10.5.0.2:8091 --username Administrator \--password password
ns_1@10.5.0.2 10.5.0.2:8091 healthy active
ns_1@10.5.0.3 10.5.0.3:8091 healthy active
ns_1@10.5.0.4 10.5.0.4:8091 healthy active
```

![alt text](https://gitlab.com/atilla59/sys-bootcamp/-/raw/master/Database/images/ss1.png)

## Pyhton App
flask yardımı ile basit bir server kurularak http basic auth ile couchbase master serverın rest apilarını kullanrak bazı dataları json şeklinde main root ("/") üstünden return ediyoruz

Örn:
```
{
balanced: true,
clusterName: "Server 1",
connectedNodes: [
        "10.5.0.2:8091",
        "10.5.0.3:8091",
        "10.5.0.4:8091"
        ]
}
```

*Example Docker compose results*
```
couchbaseServer2 | Starting Couchbase Server -- Web UI available at http://<ip>:8091
couchbaseServer2 | and logs available in /opt/couchbase/var/lib/couchbase/logs
couchbaseServer3 | Starting Couchbase Server -- Web UI available at http://<ip>:8091
couchbaseServer3 | and logs available in /opt/couchbase/var/lib/couchbase/logs
pythonapp_1   |  * Serving Flask app "server" (lazy loading)
pythonapp_1   |  * Environment: production
pythonapp_1   |    WARNING: This is a development server. Do not use it in a production deployment.
pythonapp_1   |    Use a production WSGI server instead.
pythonapp_1   |  * Debug mode: off
pythonapp_1   |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
couchbaseServer1 | Starting Couchbase Server -- Web UI available at http://<ip>:8091
couchbaseServer1 | and logs available in /opt/couchbase/var/lib/couchbase/logs
pythonapp_1   | 10.5.0.1 - - [27/Apr/2021 18:35:52] "GET / HTTP/1.1" 200 -
pythonapp_1   | 10.5.0.1 - - [27/Apr/2021 18:41:02] "GET / HTTP/1.1" 200 -
```

