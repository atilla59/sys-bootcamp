from flask import Flask
from flask import jsonify
from base64 import b64encode
import requests
import json


class MyModel:
    clusterName: str
    connectedNodes: []
    balanced: str

    def __init__(self, clusterName: str, connectedNodes: [], balanced: str) -> None:
        self.clusterName = clusterName
        self.connectedNodes = connectedNodes
        self.balanced = balanced

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


server = Flask(__name__)


@server.route("/")
def hello():

    request = requests.get(
        'http://10.5.0.2:8091/pools/default', auth=('Administrator', 'password'))

    jsonResult = request.json()

    connectedNodes = []
    for i in jsonResult['nodes']:
        connectedNodes.append(i['hostname'])

    print(jsonResult['name'], connectedNodes)

    MyModelObject = MyModel(
        jsonResult['clusterName'], connectedNodes, jsonResult['balanced'])

    return MyModelObject.toJSON()


if __name__ == "__main__":
    server.run(host='0.0.0.0')
